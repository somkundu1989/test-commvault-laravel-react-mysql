# ##########################################################################################################
## Steps used for development

# 1. Migration
php artisan make:migration create_posts_table --table=posts

# 2. Run Migration
php artisan migrate 

# 3. Create a new Post model.
php artisan make:model Post

# 4. Create a new Base controller.
php artisan make:controller Api/BaseController

# 5. Create a new Post controller.
php artisan make:controller Api/PostController --model=Post --resource

# 6. Create a new Post resource.
php artisan make:resource Api/PostResource


# ##########################################################################################################
## Steps Essential for first time

# step for laravel
1. sudo chown -R :www-data laravel/bootstrap/ 

2. sudo chown -R :www-data laravel/storage/

3. sudo chmod 775 -R laravel/storage/

5. sudo chmod 775 -R laravel/bootstrap/

# First time
docker-compose up -d --build

# use migration to create table
1. docker exec -it test-commvault-laravel-react-mysql_php-fpm_1 bash

2. cd laravel 

3. php artisan migrate


# ##########################################################################################################
## Steps rest of the other time
docker-compose up -d

