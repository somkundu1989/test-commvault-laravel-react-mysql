// import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Listing from './components/Index';
import Insert from './components/Insert';
import Edit from './components/Edit';
import View from './components/View';


function App() {

  return (
    <div className="App">
      <h1>Crud Applicaton</h1>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Listing/>}></Route>
          <Route path='/create' element={<Insert/>}></Route>
          <Route path='/details/:objectid' element={<View/>}></Route>
          <Route path='/edit/:objectid' element={<Edit/>}></Route>
          
        </Routes>
      </BrowserRouter>
    </div>
  );

}

export default App;
