import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { BaseUrl } from "../constants/Global";

const Edit = () => {

    const { objectid } = useParams();
    const [id, idchange] = useState("");
    const [title, titlechange] = useState("");
    const [body, bodychange] = useState("");
    const [userId, userIdchange] = useState("");
    const [validation, valchange] = useState(false);
    const navigate = useNavigate();
   
    useEffect(() => {
        fetch(BaseUrl + 'posts/' + objectid).then((res) => {
            return res.json();
        }).then((resp) => {
            if (resp.success === true){
                idchange(resp.data.id);
                titlechange(resp.data.title);
                bodychange(resp.data.body);
                userIdchange(resp.data.userId);
            } else {
                alert('Not fetched. check Console');
                console.log(resp);
            }
        }).catch((err) => {
            console.log(err.messaeg);
        })
    }, []);

    const handelsubmit = (e) => {
        e.preventDefault();
        const objectdata = { id, title, body, userId};
        fetch(BaseUrl + 'posts/' + objectid, {
            method: "PUT",
            headers: { "content-type": "application/json" },
            body: JSON.stringify(objectdata)
        }).then((res) => {
            if (res.status == 200){
                alert('updated successfully.')
                navigate('/')
            } else {
                alert('Not updated. check Console');
                console.log(res);
            }
        }).catch((err) => {
            console.log(err.message);
        })
    }

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <form className="card mt-3" onSubmit={handelsubmit}>
                        <div className="card-title mt-3">
                            <h2>Post Edit</h2>
                        </div>
                        <div className="card-body">
                            <div className="form-grop text-start mb-3">
                                <label>Id</label>
                                <input value={id} disabled="disabled" className="form-control" placeholder="Id"></input>
                            </div>
                            <div className="form-grop text-start mb-3">
                                <label>Title</label>
                                <input required value={title} onMouseDown={e => valchange(true)} onChange={e => titlechange(e.target.value)} className="form-control" placeholder="Title"></input>
                                {title.length == 0 && validation && <span className="text-danger">Enter the title</span>}
                            </div>
                            <div className="form-grop text-start mb-3">
                                <label>Details</label>
                                <textarea required value={body} onMouseDown={e => valchange(true)} onChange={e => bodychange(e.target.value)} rows="5" className="form-control" placeholder="Enter Details"></textarea>
                                {body.length == 0 && validation && <span className="text-danger">Enter your messaeg</span>}
                            </div>
                            <div className="form-grop text-start mb-3">
                                <label>User ID</label>
                                <input required value={userId} onMouseDown={e => valchange(true)} onChange={e => userIdchange(e.target.value)} className="form-control" placeholder="User ID"></input>
                                {userId.length == 0 && validation && <span className="text-danger">Enter the User ID</span>}
                            </div>
                            <div className="form-grop text-start d-flex justify-content-between  mb-3">
                                <Link to="/" className="btn btn-info">(←) Back</Link>
                                <button type="submit" className="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default Edit;