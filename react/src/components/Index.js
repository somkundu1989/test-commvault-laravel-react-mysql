import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { BaseUrl } from "../constants/Global";

const Listing = () => {
    const [listData , listDataChange] = useState(null);
    const navigate = useNavigate();
    const LoadDetails = (id) => {
        navigate("/details/" + id);
    }
    const LoadEdit = (id) => {
        navigate("/edit/" + id);
    }
    const RemoveFunction = async (id) => {
        if (window.confirm('Do you want to remove?')) {
            await fetch(BaseUrl + 'posts/' + id, {
                method: "DELETE"
            }).then((res) => {
                alert('Removed successfully.')
                window.location.reload();
                console.log(res);
            }).catch((err) => {
                console.log(err.message);
            })
        }
    }

    useEffect(() => {
        fetch(BaseUrl + 'posts/').then((res) => {
            return res.json();
        }).then((resp) => {
            if (resp.success === true){
                listDataChange(resp.data);
            } else {
                alert('Not fetched. check Console');
                console.log(resp);
            }            
        }).catch((err) => {
            console.log(err.messaeg);
        })
    }, [])

    return (
        <div className="container">
            <div className="card mt-4">
                <div className="card-title mt-3">
                    <h1>POst Listing</h1>
                </div>
                <div className="card-body text-start">
                    <div className="d-flex mb-1">
                        <Link to="/create" className="btn btn-success">Add New (+)</Link>
                    </div>
                    <table className="table table-bordered table-fixed">
                        <thead className="bg-dark  text-white">
                            <tr>
                                <td width={70}>Post Id</td>
                                <td width={180}>Title</td>
                                <td width={370}>Body Copy</td>
                                <td width={180}>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            {listData &&
                                listData.map(item => (
                                    <tr key={item.id}>
                                        <td>{item.id}</td>
                                        <td>{item.title}</td>
                                        <td>{item.body}</td>
                                        <td>
                                            <a onClick={() => { LoadEdit(item.id) }} className="btn btn-info">Edit</a>
                                            <a onClick={() => { LoadDetails(item.id) }} className="btn btn-primary">Details</a>
                                            <a onClick={() => { RemoveFunction(item.id) }} className="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default Listing;
