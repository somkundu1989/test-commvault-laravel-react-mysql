import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { BaseUrl } from "../constants/Global";

const View = () => {

    const { objectid } = useParams();
    const [objectdata, objectdatachange] = useState({});

    useEffect(() => {
        fetch(BaseUrl + 'posts/' + objectid).then((res) => {
            return res.json();
        }).then((resp) => {
            if (resp.success === true){
                objectdatachange(resp.data);
            } else {
                alert('Not fetched. check Console');
                console.log(resp);
            }
        }).catch((err) => {
            console.log(err.messaeg);
        })
    }, []);

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-6 mt-4">
                    <div className="card">
                        <div className="card-title mt-3">
                            <h2>Post single data</h2>
                        </div>
                        <div className="card-body">

                            {objectdata &&
                                <div className="text-start">
                                    <p><b>ID : {objectdata.id}</b></p>
                                    <p> <b>Title :</b> {objectdata.title} </p>
                                    <p><b>Body :</b> {objectdata.body} </p>
                                    <p><b>User ID :</b> {objectdata.userId} </p>
                                </div>
                            }

                            <div className="form-grop text-start d-flex justify-content-between  mb-3">
                                <Link to="/" className="btn btn-info">(←) Back</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default View;