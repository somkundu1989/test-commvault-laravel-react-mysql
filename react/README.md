# create the server image run
docker build -f Dockerfile -t client .

# create a container to execute the server image
docker run -itd -p 4001:3000 client

